package homeworkaverage2;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Average2 {
	
	private String fileRead;
	private String fileWrite;
	private FileReader reader;
	private FileWriter writer;
	private double sum = 0;
	private int n = 0;
	
	Average2(String fileRead, String fileWrite){
		this.fileRead = fileRead;
		this.fileWrite = fileWrite;
	}

	public void averageScore(){
	
		try {
			reader = new FileReader(fileRead);
			BufferedReader buffer = new BufferedReader(reader);
			
			writer = new FileWriter(fileWrite,true);
			PrintWriter out = new PrintWriter(new BufferedWriter(writer));
			
			out.println("-----------Exam Score--------------"+"\nName   Average"+"\n====   =======");
		
			for (String line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] score = line.split(",");
				out.print(score[0]);
				for(int i=1;i<=score.length-1;i++){
					double point = Double.parseDouble(score[i]);
					sum = sum+point;
					n++;
				}
				out.println("\t"+sum/n);
				n = 0;
				sum = 0;
			}
			out.flush();

		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+fileRead);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (reader != null)
					reader.close();
			} 
			catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
}
