package phonebook;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class PhoneBook {
	
	public void phone(){
		String filename = "phonebook.txt";
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);	
			
			String line;
			for(line = buffer.readLine(); line != null; line = buffer.readLine())
				System.out.println(line);
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
}
